﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[ExecuteInEditMode]
public class PostScript : MonoBehaviour
{
    public Material mat;
    bool active = false;
    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (active) 
            Graphics.Blit(src, dest, mat);
        else
        {
            Graphics.Blit(src, dest);
        }
    }

    private void Update()
    {
        if (Input.GetButtonUp("Fire1"))
        {
            active = !active;
        }
    }
}
