﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate1 : MonoBehaviour
{
    public GameObject center;
    void FixedUpdate()
    {
        transform.RotateAround(center.transform.position, Vector3.up, 20 * Time.deltaTime);
    }
}